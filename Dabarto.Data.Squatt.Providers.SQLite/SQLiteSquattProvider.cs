﻿using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Security;

namespace Dabarto.Data.Squatt.Data.Providers
{
    public class SQLiteSquattProvider : SquattProvider
    {
        public override string IdentifierEnclosingStartChar
        {
            get
            {
                return "[";
            }
        }

        public override string IdentifierEnclosingEndChar
        {
            get
            {
                return "]";
            }
        }

        public SQLiteSquattProvider()
        {
        }

        public override string GetSelectQuery(string tableName, string keyFieldName, long id, List<string> fieldNames)
        {
            return string.Format("{0} WHERE {1} = {2}", GetGeneralSelectQuery(tableName, keyFieldName, fieldNames), GetQuotedIdentifierName(keyFieldName), id);
        }

        public override string GetSelectAllQuery(string tableName, string keyFieldName, List<string> fieldNames, string orderColumn, OrderDirection? orderDirection, int? offset, int? limit)
        {
            var orderBy = string.Empty;
            var limitClause = string.Empty;
            if (!string.IsNullOrWhiteSpace(orderColumn))
            {
                orderBy += " ORDER BY " + orderColumn;
                if (orderDirection.HasValue)
                {
                    orderBy += " " + (orderDirection == OrderDirection.Descending ? "DESC" : "ASC");
                }
            }
            if (limit.HasValue)
            {
                limitClause += " LIMIT ";
                if (offset.HasValue)
                {
                    limitClause += offset + ", ";
                }
                limitClause += limit;
            }
            return string.Format("{0}{1}{2}", GetGeneralSelectQuery(tableName, keyFieldName, fieldNames), orderBy, limitClause);
        }

        public override string GetSelectConditionalQuery(string tableName, string keyFieldName, List<string> fieldNames, string condition, string orderColumn, OrderDirection? orderDirection, int? offset, int? limit)
        {
            var orderBy = string.Empty;
            var limitClause = string.Empty;
            if (!string.IsNullOrWhiteSpace(orderColumn))
            {
                orderBy += " ORDER BY " + orderColumn;
                if (orderDirection.HasValue)
                {
                    orderBy += " " + (orderDirection == OrderDirection.Descending ? "DESC" : "ASC");
                }
            }
            if (limit.HasValue)
            {
                limitClause += " LIMIT ";
                if (offset.HasValue)
                {
                    limitClause += offset + ", ";
                }
                limitClause += limit;
            }
            return string.Format("{0} WHERE {1}{2}{3}", GetGeneralSelectQuery(tableName, keyFieldName, fieldNames), condition, orderBy, limitClause);
        }

        public override int ExecuteNonQuery(string query)
        {
            using (var connection = new SQLiteConnection(Configuration.ConnectionString))
            {
                connection.Open();
                var command = connection.CreateCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = query;
                var res = command.ExecuteNonQuery();
                connection.Close();
                return res;
            }
        }

        public override DataTable ExecuteQuery(string query)
        {
            DataTable dataTable = new DataTable();

            using (var connection = new SQLiteConnection(Configuration.ConnectionString))
            {
                var adapter = new SQLiteDataAdapter(query, connection);
                adapter.Fill(dataTable);
            }

            return dataTable;
        }

        public override long PerformInsert(string query)
        {
            using (var connection = new SQLiteConnection(Configuration.ConnectionString))
            {
                connection.Open();
                var transaction = connection.BeginTransaction();

                try
                {
                    var command = connection.CreateCommand();
                    command.CommandType = CommandType.Text;
                    command.CommandText = query;
                    command.ExecuteNonQuery();

                    command.CommandText = "SELECT LAST_INSERT_ROWID();";
                    var result = command.ExecuteScalar();

                    transaction.Commit();

                    return long.Parse(result.ToString());
                }
                catch
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }

        public override void TruncateTable(string tableName)
        {
            ExecuteNonQuery(string.Format("DELETE FROM {0}{1}{2}", IdentifierEnclosingStartChar, EscapeString(tableName), IdentifierEnclosingEndChar));
            ExecuteNonQuery(string.Format("DELETE FROM SQLITE_SEQUENCE WHERE name='{0}'", EscapeString(tableName)));
        }

        public override string EscapeString(string str)
        {
            return SecurityElement.Escape(str);
        }
    }
}