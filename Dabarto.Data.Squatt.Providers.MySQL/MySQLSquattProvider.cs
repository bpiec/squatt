﻿using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Data;

namespace Dabarto.Data.Squatt.Data.Providers
{
    public class MySQLSquattProvider : SquattProvider
    {
        public override string IdentifierEnclosingStartChar
        {
            get
            {
                return "`";
            }
        }

        public override string IdentifierEnclosingEndChar
        {
            get
            {
                return "`";
            }
        }

        public MySQLSquattProvider()
        {
        }

        public override string GetSelectQuery(string tableName, string keyFieldName, long id, List<string> fieldNames)
        {
            return string.Format("{0} WHERE {1} = {2}", GetGeneralSelectQuery(tableName, keyFieldName, fieldNames), GetQuotedIdentifierName(keyFieldName), id);
        }

        public override string GetSelectAllQuery(string tableName, string keyFieldName, List<string> fieldNames, string orderColumn, OrderDirection? orderDirection, int? offset, int? limit)
        {
            var orderBy = string.Empty;
            var limitClause = string.Empty;
            if (!string.IsNullOrWhiteSpace(orderColumn))
            {
                orderBy += " ORDER BY " + orderColumn;
                if (orderDirection.HasValue)
                {
                    orderBy += " " + (orderDirection == OrderDirection.Descending ? "DESC" : "ASC");
                }
            }
            if (limit.HasValue)
            {
                limitClause += " LIMIT ";
                if (offset.HasValue)
                {
                    limitClause += offset + ", ";
                }
                limitClause += limit;
            }
            return string.Format("{0}{1}{2}", GetGeneralSelectQuery(tableName, keyFieldName, fieldNames), orderBy, limitClause);
        }

        public override string GetSelectConditionalQuery(string tableName, string keyFieldName, List<string> fieldNames, string condition, string orderColumn, OrderDirection? orderDirection, int? offset, int? limit)
        {
            var orderBy = string.Empty;
            var limitClause = string.Empty;
            if (!string.IsNullOrWhiteSpace(orderColumn))
            {
                orderBy += " ORDER BY " + orderColumn;
                if (orderDirection.HasValue)
                {
                    orderBy += " " + (orderDirection == OrderDirection.Descending ? "DESC" : "ASC");
                }
            }
            if (limit.HasValue)
            {
                limitClause += " LIMIT ";
                if (offset.HasValue)
                {
                    limitClause += offset + ", ";
                }
                limitClause += limit;
            }
            return string.Format("{0} WHERE {1}{2}{3}", GetGeneralSelectQuery(tableName, keyFieldName, fieldNames), condition, orderBy, limitClause);
        }

        public override int ExecuteNonQuery(string query)
        {
            using (var connection = new MySqlConnection(Configuration.ConnectionString))
            {
                connection.Open();
                var command = connection.CreateCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = query;
                return command.ExecuteNonQuery();
            }
        }

        public override DataTable ExecuteQuery(string query)
        {
            var dataTable = new DataTable();

            using (var connection = new MySqlConnection(Configuration.ConnectionString))
            {
                var adapter = new MySqlDataAdapter(query, connection);
                adapter.Fill(dataTable);
            }

            return dataTable;
        }

        public override long PerformInsert(string query)
        {
            using (var connection = new MySqlConnection(Configuration.ConnectionString))
            {
                connection.Open();
                var transaction = connection.BeginTransaction();

                try
                {
                    var command = connection.CreateCommand();
                    command.CommandType = CommandType.Text;
                    command.CommandText = query;
                    command.ExecuteNonQuery();

                    command.CommandText = "SELECT LAST_INSERT_ID();";
                    var result = command.ExecuteScalar();

                    transaction.Commit();

                    return long.Parse(result.ToString());
                }
                catch
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }

        public override void TruncateTable(string tableName)
        {
            ExecuteNonQuery(string.Format("TRUNCATE TABLE {0}{1}{2}", IdentifierEnclosingStartChar, EscapeString(tableName), IdentifierEnclosingEndChar));
        }

        public override string EscapeString(string str)
        {
            return MySqlHelper.EscapeString(str);
        }
    }
}