using System;

namespace Dabarto.Data.Squatt.Data.Attributes
{
    /// <summary>
    /// An attribute that can be placed on a class to specify a database table name.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class DbTableAttribute : Attribute
    {
        private string _tableName;
        private string _tableNamePropertyProvider;

        /// <summary>
        /// Gets or sets a database table name.
        /// </summary>
        public string TableName
        {
            get
            {
                return _tableName;
            }
            set
            {
                _tableName = value;
            }
        }

        /// <summary>
        /// Gets or sets a name of property that will provide the table name.
        /// </summary>
        public string TableNamePropertyProvider
        {
            get
            {
                return _tableNamePropertyProvider;
            }
            set
            {
                _tableNamePropertyProvider = value;
            }
        }

        /// <summary>
        /// Creates a new table attribute.
        /// </summary>
        /// <param name="tableName">A database table name.</param>
        public DbTableAttribute(string tableName)
        {
            _tableName = tableName;
        }

        /// <summary>
        /// Creates a new table attribute.
        /// </summary>
        /// <param name="tableName">A database table name.</param>
        /// <param name="tableNamePropertyProvider">A name of property that will provide the table name.</param>
        public DbTableAttribute(string tableName, string tableNamePropertyProvider)
        {
            _tableName = tableName;
            _tableNamePropertyProvider = tableNamePropertyProvider;
        }
    }
}