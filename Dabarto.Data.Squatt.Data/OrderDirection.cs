﻿namespace Dabarto.Data.Squatt.Data
{
    public enum OrderDirection
    {
        Ascending,
        Descending
    }
}